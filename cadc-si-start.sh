#!/bin/bash

# Pull all the containers precreated
docker pull amigahub/cadc-postgresql
docker pull amigahub/cadc-haproxy
docker pull amigahub/minoc
docker pull amigahub/luskan
docker pull amigahub/fenwick
docker pull amigahub/crtiwall

# Default config files locate in ${CONFIG_FOLDER}
$CONFIG_FOLDER="/Users/manuparra/repos/docker-base/"


# Instantiate cadc-postgresql
docker run -d --volume=${CONFIG_FOLDER}/config/postgresql:/config:ro \
              --volume=${CONFIG_FOLDER}/config/postgresql-logs:/logs:rw \
              -p 5432:5432 
              --name pg10db 
              cadc-postgresql:latest

# Wait to populate
sleep 7

# Instantiate cadc-minoc
docker run -d --user tomcat:tomcat  --link pg10db:pg10db \
              --volume=${CONFIG_FOLDER}/config/minoc:/config:ro 
              --name minoc 
              minoc:latest

#Wait
sleep 3

# Instantiate cadc-luskan
docker run -d --user tomcat:tomcat  --link pg10db:pg10db \
              --volume=${CONFIG_FOLDER}/config/luskan:/config:ro 
              --name luskan 
              luskan:latest
#Wait 
sleep 3
# Instantiate cadc-fenwick
docker run -d --user tomcat:tomcat  --link pg10db:pg10db \
              --volume=${CONFIG_FOLDER}/config/fenwick:/config:ro 
              --name fenwick 
              fenwick:latest
#Wait 
sleep 3
# Instantiate cadc-critwall
docker run -d --user tomcat:tomcat  --link pg10db:pg10db \
              --volume=${CONFIG_FOLDER}/config/critwall:/config:ro 
              --name critwall 
              critwall:latest


#Create you own ssl cert without manual entry
openssl genrsa -out spsrc.key 2048
openssl req -new -subj "/C=ES/ST=Spain/L=Granada/O=IAA-CSIC/CN=localhost" -key spsrc.key -out spsrc.csr
openssl x509 -req rsa:2048 -days 365 -in spsrc.csr -signkey spsrc.key -out spsrc.crt
cat spsrc.key spsrc.crt >> ${CONFIG_FOLDER}/config/server-cert.pem


# Instantiate cadc-haproxy
docker run -d --volume=${CONFIG_FOLDER}/config/haproxy/logs:/logs:rw \
              --volume=${CONFIG_FOLDER}/config/certs/:/config:ro \
			  --volume=${CONFIG_FOLDER}/config/haproxy/config:/usr/local/etc/haproxy/:rw \
              --link minoc:minoc \
              --link luskan:luskan \
              --link critwall:critwall \
              --link fenwick:fenwick \
              -p 8443:443 \
              --name haproxy \
              cadc-haproxy:latest

